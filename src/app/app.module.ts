import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {ReactiveFormsModule} from '@angular/forms';
import { MatNativeDateModule } from '@angular/material/core';

import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuthModule } from "@angular/fire/auth";
import { AngularFireAuthGuardModule } from "@angular/fire/auth-guard";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SharedModule } from './modules/shared/shared.module';
import { environment } from 'src/environments/environment';
import { LoginPageComponent } from './pages/login-page/login-page.component';
import { DashboardPageComponent } from './pages/dashboard-page/dashboard-page.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { AddIssueDialogComponent } from './components/add-issue-dialog/add-issue-dialog.component';
import { IssueFormComponent } from './components/issue-form/issue-form.component';
import { IssueComponent } from './components/issue/issue.component';
import { IssueListComponent } from './components/issue-list/issue-list.component';
import { EditIssueDialogComponent } from './components/edit-issue-dialog/edit-issue-dialog.component';
import { ToDatePipe } from './pipes/to-date.pipe';
import { ConfirmDeleteIssueDialogComponent } from './components/confirm-delete-issue-dialog/confirm-delete-issue-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginPageComponent,
    DashboardPageComponent,
    DashboardComponent,
    AddIssueDialogComponent,
    IssueFormComponent,
    IssueComponent,
    IssueListComponent,
    EditIssueDialogComponent,
    ToDatePipe,
    ConfirmDeleteIssueDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    MatNativeDateModule,
    SharedModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireAuthModule,
    AngularFireAuthGuardModule
  ],
  entryComponents: [
    AddIssueDialogComponent,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
