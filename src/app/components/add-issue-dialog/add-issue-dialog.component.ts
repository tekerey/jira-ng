import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

import { IssueStatus } from 'src/app/enums/issueStatus.enum';
import { IIssue } from 'src/app/interfaces/issue.dto';
import { IssueFormComponent } from '../issue-form/issue-form.component';

@Component({
  selector: 'app-add-issue-dialog',
  templateUrl: './add-issue-dialog.component.html',
  styleUrls: ['./add-issue-dialog.component.scss']
})
export class AddIssueDialogComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<AddIssueDialogComponent>,
    private firestore: AngularFirestore,
    private auth: AngularFireAuth,
    private router: Router,
    private _snackBar: MatSnackBar,
  ) { }

  ngOnInit(): void {
  }

  async addIssue(issueForm: IssueFormComponent): Promise<void> {
    issueForm.form.markAllAsTouched();
    
    if (issueForm.form.valid) {
      this.auth.user.subscribe(async user => {
        if (!user) {
          await this.auth.signOut();
          this.router.navigateByUrl('login');
          return;
        }
        try {
          const id = this.firestore.createId();
          await this.firestore.doc<IIssue>(`issues/${id}`).set({
            id: id,
            type: issueForm.typeControl.value,
            assignee: issueForm.assigneeControl.value || null,
            reporter: user.uid,
            dueDate: issueForm.dueDateControl.value,
            summary: issueForm.summaryControl.value,
            description: issueForm.descriptionControl.value,
            status: IssueStatus.ToDo,
            index: -1,
          });

          this.dialogRef.close(true);
          this._snackBar.open('Issue has been created', 'Close', {
            horizontalPosition: 'right',
            verticalPosition: 'bottom',
            duration: 8000,
          });
        } catch (error) {
          console.error(error);
          this._snackBar.open('Error during issue creation', 'Close', {
            horizontalPosition: 'right',
            verticalPosition: 'bottom',
            duration: 8000,
            panelClass: 'snackbar_error',
          });
        }
      });
    }
  }
}
