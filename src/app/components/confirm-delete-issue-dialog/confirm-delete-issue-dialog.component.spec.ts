import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmDeleteIssueDialogComponent } from './confirm-delete-issue-dialog.component';

describe('ConfirmDeleteIssueDialogComponent', () => {
  let component: ConfirmDeleteIssueDialogComponent;
  let fixture: ComponentFixture<ConfirmDeleteIssueDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConfirmDeleteIssueDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmDeleteIssueDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
