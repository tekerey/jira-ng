import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-confirm-delete-issue-dialog',
  templateUrl: './confirm-delete-issue-dialog.component.html',
  styleUrls: ['./confirm-delete-issue-dialog.component.scss']
})
export class ConfirmDeleteIssueDialogComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<ConfirmDeleteIssueDialogComponent>,) { }

  ngOnInit(): void {
  }

  deleteIssue(): void {
    this.dialogRef.close(true);
  }

}
