import { Component, OnDestroy, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Observable, Subscription } from 'rxjs';

import { IIssue } from 'src/app/interfaces/issue.dto';
import { IssueStatus } from 'src/app/enums/issueStatus.enum';
import { AddIssueDialogComponent } from '../add-issue-dialog/add-issue-dialog.component';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { AngularFireAuth } from '@angular/fire/auth';
import { FormControl } from '@angular/forms';
import { IssueType } from 'src/app/enums/type.enum';
import { EditIssueService } from 'src/app/services/edit-issue.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, OnDestroy {

  issues: IIssue[] = [];
  filteredIssues: IIssue[] = [];

  issueToDo: IIssue[] = [];
  issueInProgress: IIssue[] = [];
  issueReview: IIssue[] = [];
  issueDone: IIssue[] = [];

  statuses = {
    todo: IssueStatus.ToDo,
    in: IssueStatus.InProgress,
    review: IssueStatus.Review,
    done: IssueStatus.Done,
  }

  onlyCreatedByMe: boolean = false;
  onlyAssignedOnMe: boolean = false;

  userId: string | undefined = '';

  // type select
  typeControl: FormControl = new FormControl('');
  typeArray = [IssueType.Epic, IssueType.UserStory, IssueType.Task, IssueType.Bug];

  issueUpdatedSubscription: Subscription;
  issueDeletedSubscription: Subscription;

  constructor(
    private dialog: MatDialog,
    private firestore: AngularFirestore,
    private auth: AngularFireAuth,
    private editIssueService: EditIssueService,
  ) {
    this.updateIssues();

    this.issueUpdatedSubscription = this.editIssueService.issueUpdated.subscribe(updated => {
      if (updated) {
        this.updateFilters();
      }
    });
    this.issueDeletedSubscription = this.editIssueService.issueDeleted.subscribe(deleted => {
      if (deleted) {
        this.updateIssues();
      }
    })
  }

  ngOnInit(): void {
    this.auth.user.subscribe(user => {
      this.userId = user?.uid;
    });

    this.typeControl.valueChanges.subscribe(() => this.updateFilters());
  }

  ngOnDestroy(): void {
    this.issueUpdatedSubscription.unsubscribe();
    this.issueDeletedSubscription.unsubscribe();
  }

  updateIssues() {
    this.firestore.collection<IIssue>('issues').get().subscribe(query => {
      this.issues = query.docs.map(doc => doc.data());
      this.updateFilters();
    });
  }

  splitIssues(issues: IIssue[]) {
    this.issueToDo = issues.filter(i => i.status === IssueStatus.ToDo).sort((a, b) => a.index - b.index);
    this.issueInProgress = issues.filter(i => i.status === IssueStatus.InProgress).sort((a, b) => a.index - b.index);
    this.issueReview = issues.filter(i => i.status === IssueStatus.Review).sort((a, b) => a.index - b.index);
    this.issueDone = issues.filter(i => i.status === IssueStatus.Done).sort((a, b) => a.index - b.index);
  }

  updateFilters() {
    this.filteredIssues = this.issues;
    
    if (this.onlyCreatedByMe) {
      this.filteredIssues = this.filteredIssues.filter(i => i.reporter === this.userId);
    }

    if (this.onlyAssignedOnMe) {
      this.filteredIssues = this.filteredIssues.filter(i => i.assignee === this.userId);
    }

    if (this.typeControl.value) {
      this.filteredIssues = this.filteredIssues.filter(i => i.type === this.typeControl.value);
    }

    this.splitIssues(this.filteredIssues);
  }

  handleCreatedByMe(event: MatSlideToggleChange) {
    this.onlyCreatedByMe = event.checked;
    this.updateFilters();
  }

  handleAssignedOnMe(event: MatSlideToggleChange) {
    this.onlyAssignedOnMe = event.checked;
    this.updateFilters();
  }

  openAddNewIssueDialog(): void {
    const dialogRef = this.dialog.open(AddIssueDialogComponent, {
      width: '400px',
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed: ' + result);
      if (result) {
        this.updateIssues();
      }
    });
  }
}
