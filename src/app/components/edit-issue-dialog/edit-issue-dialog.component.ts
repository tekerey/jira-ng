import { Component, Inject, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

import { IIssue } from 'src/app/interfaces/issue.dto';
import { EditIssueService } from 'src/app/services/edit-issue.service';
import { ConfirmDeleteIssueDialogComponent } from '../confirm-delete-issue-dialog/confirm-delete-issue-dialog.component';
import { IssueFormComponent } from '../issue-form/issue-form.component';

@Component({
  selector: 'app-edit-issue-dialog',
  templateUrl: './edit-issue-dialog.component.html',
  styleUrls: ['./edit-issue-dialog.component.scss']
})
export class EditIssueDialogComponent implements OnInit {

  canDelete: boolean = false;

  constructor(
    public dialogRef: MatDialogRef<EditIssueDialogComponent>,
    private firestore: AngularFirestore,
    private auth: AngularFireAuth,
    private router: Router,
    private _snackBar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) public issue: IIssue,
    private dialog: MatDialog,
    private editIssueService: EditIssueService,
  ) {
    this.auth.user.subscribe(user => {
      if (user?.uid === this.issue.reporter) {
        this.canDelete = true;
      }
    });
  }

  ngOnInit(): void {
  }

  async saveIssue(issueForm: IssueFormComponent): Promise<void> {
    issueForm.form.markAllAsTouched();
    
    if (issueForm.form.valid) {
      this.auth.user.subscribe(async user => {
        if (!user) {
          await this.auth.signOut();
          this.router.navigateByUrl('login');
          return;
        }
        try {
          await this.firestore.doc<IIssue>(`issues/${this.issue.id}`).update({
            type: issueForm.typeControl.value,
            assignee: issueForm.assigneeControl.value || null,
            dueDate: issueForm.dueDateControl.value,
            summary: issueForm.summaryControl.value,
            description: issueForm.descriptionControl.value,
            status: issueForm.statusControl.value,
          });

          // Update local object
          this.issue.type = issueForm.typeControl.value;
          this.issue.assignee = issueForm.assigneeControl.value || null;
          this.issue.dueDate = { seconds: (issueForm.dueDateControl.value as Date).getTime() / 1000 };
          this.issue.summary = issueForm.summaryControl.value;
          this.issue.description = issueForm.descriptionControl.value;
          this.issue.status = issueForm.statusControl.value;

          this.dialogRef.close(true);
          this._snackBar.open('Issue has been updated', 'Close', {
            horizontalPosition: 'right',
            verticalPosition: 'bottom',
            duration: 8000,
          });
        } catch (error) {
          console.error(error);
          this._snackBar.open('Error during issue updating', 'Close', {
            horizontalPosition: 'right',
            verticalPosition: 'bottom',
            duration: 8000,
            panelClass: 'snackbar_error',
          });
        }
      });
    }
  }

  deleteIssue() {
    this.auth.user.subscribe(async user => {
        if (!user) {
          await this.auth.signOut();
          this.router.navigateByUrl('login');
          return;
        }
          // Open confirm delete dialog
          const dialogRef = this.dialog.open(ConfirmDeleteIssueDialogComponent);
      
          dialogRef.afterClosed().subscribe(async result => {
            if (result) {
              // Delete issue
              try {
                await this.firestore.doc<IIssue>(`issues/${this.issue.id}`).delete();
      
                this.dialogRef.close();
                this.editIssueService.issueDeleted.next(true);
                this._snackBar.open('Issue has been deleted', 'Close', {
                  horizontalPosition: 'right',
                  verticalPosition: 'bottom',
                  duration: 8000,
                });
              } catch (error) {
                console.error(error);
                this._snackBar.open('Error during issue deleting', 'Close', {
                  horizontalPosition: 'right',
                  verticalPosition: 'bottom',
                  duration: 8000,
                  panelClass: 'snackbar_error',
                });
              }
            }
          });

      });
  }
}
