import { Component, Input, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { IssueStatus } from 'src/app/enums/issueStatus.enum';

import { IssueType } from 'src/app/enums/type.enum';
import { IIssue } from 'src/app/interfaces/issue.dto';
import { IUser } from 'src/app/interfaces/user.dto';

@Component({
  selector: 'app-issue-form',
  templateUrl: './issue-form.component.html',
  styleUrls: ['./issue-form.component.scss']
})
export class IssueFormComponent implements OnInit {

  @Input() issue: IIssue | undefined;

  form: FormGroup;
  typeControl = new FormControl('', [Validators.required]);
  assigneeControl = new FormControl('');
  dueDateControl = new FormControl('', [Validators.required]);
  summaryControl = new FormControl('', [Validators.required, Validators.maxLength(100)]);
  descriptionControl = new FormControl('', [Validators.maxLength(1000)]);


  types: IssueType[] = [ IssueType.Epic, IssueType.UserStory, IssueType.Task, IssueType.Bug ];
  users: Observable<IUser[]>;
  minDate: Date = new Date();

  // For edit form
  reporterControl = new FormControl({value: '', disabled: true});
  statusControl = new FormControl('', [Validators.required]);
  statuses = [IssueStatus.ToDo, IssueStatus.InProgress, IssueStatus.Review, IssueStatus.Done];

  constructor(private firestore: AngularFirestore) {
    this.form = new FormGroup({
      type: this.typeControl,
      assignee: this.assigneeControl,
      dueDate: this.dueDateControl,
      summary: this.summaryControl,
      description: this.descriptionControl,
    });

    this.users = this.firestore.collection<IUser>('users').valueChanges();
  }

  ngOnInit(): void {
    if (this.issue) {
      this.form.addControl('reporter', this.reporterControl);
      this.form.addControl('status', this.statusControl);

      this.form.reset({
        type: this.issue.type,
        assignee: this.issue.assignee,
        dueDate: new Date(this.issue.dueDate.seconds * 1000),
        summary: this.issue.summary,
        description: this.issue.description,
        reporter: this.issue.reporter,
        status: this.issue.status,
      }, {emitEvent: false});
    }
  }

}
