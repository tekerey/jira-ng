import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { Component, Input, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { IssueStatus } from 'src/app/enums/issueStatus.enum';
import { IIssue } from 'src/app/interfaces/issue.dto';

@Component({
  selector: 'app-issue-list',
  templateUrl: './issue-list.component.html',
  styleUrls: ['./issue-list.component.scss']
})
export class IssueListComponent implements OnInit {

  @Input() issues: IIssue[] = [];
  @Input() status!: IssueStatus;

  constructor(private firestore: AngularFirestore) { }

  ngOnInit(): void {

  }

  drop(event: CdkDragDrop<IIssue[]>): void {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    }

    // Save new issue's status
    const issue: IIssue = event.item.data;
    this.firestore.doc<IIssue>(`issues/${issue.id}`).update({
      status: this.status,
    });

    // Update local issue object
    issue.status = this.status;

    // Update indexes
    event.container.data.forEach((i, index) => {
      this.firestore.doc<IIssue>(`issues/${i.id}`).update({ index: index });
      if (i === issue) {
        issue.index = index;
      }
    });

    event.previousContainer.data.forEach((i, index) => {
      this.firestore.doc<IIssue>(`issues/${i.id}`).update({ index: index });
    });
  }

}
