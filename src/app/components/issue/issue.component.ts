import { Component, Input, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { MatDialog } from '@angular/material/dialog';
import { Observable } from 'rxjs';

import { IIssue } from 'src/app/interfaces/issue.dto';
import { IUser } from 'src/app/interfaces/user.dto';
import { EditIssueService } from 'src/app/services/edit-issue.service';
import { EditIssueDialogComponent } from '../edit-issue-dialog/edit-issue-dialog.component';

@Component({
  selector: 'app-issue',
  templateUrl: './issue.component.html',
  styleUrls: ['./issue.component.scss']
})
export class IssueComponent implements OnInit {

  @Input() issue!: IIssue;
  assignee$!: Observable<IUser | undefined>;
  assigneeAvatar: string | undefined | null;
  assigneeUsername: string = '';

  constructor(
    private firestore: AngularFirestore,
    private dialog: MatDialog,
    private editIssueService: EditIssueService,
  ) {
  }

  ngOnInit(): void {
    this.assignee$ = this.firestore.doc<IUser>(`users/${this.issue.assignee}`).valueChanges();
    this.assignee$.subscribe(user => {
      this.assigneeAvatar = user?.userAvatarURL;
      this.assigneeUsername = user?.username || '';
    });
  }

  openEditIssueDialog(): void {
    const dialogRef = this.dialog.open(EditIssueDialogComponent, {
      width: '400px',
      data: this.issue,
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed: ' + result);
      if (result) {
        this.editIssueService.issueUpdated.next(true);
        
        this.assignee$ = this.firestore.doc<IUser>(`users/${this.issue.assignee}`).valueChanges();
        this.assignee$.subscribe(user => {
          this.assigneeAvatar = user?.userAvatarURL;
          this.assigneeUsername = user?.username || '';
        });
      }
    });
  }

}
