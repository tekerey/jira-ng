export enum IssueStatus {
    ToDo = 'To Do',
    InProgress = 'In progress',
    Review = 'Review',
    Done = 'Done',
}