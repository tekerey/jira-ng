export enum IssueType {
    Epic = 'Epic',
    UserStory = 'User Story',
    Task = 'Task',
    Bug = 'Bug',
}