import { IssueStatus } from "../enums/issueStatus.enum";
import { IssueType } from "../enums/type.enum";

export interface IIssue {
    id: string,
    type: IssueType,
    assignee?: string,
    reporter: string,
    dueDate: any,
    summary: string,
    description?: string,
    status: IssueStatus,
    index: number,
}