export interface IUser {
    id: string,
    username: string | null,
    userAvatarURL: string | null,
}