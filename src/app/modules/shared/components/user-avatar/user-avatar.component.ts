import { Component, Input, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-user-avatar',
  templateUrl: './user-avatar.component.html',
  styleUrls: ['./user-avatar.component.scss']
})
export class UserAvatarComponent implements OnInit {

  @Input() src: string | null | undefined;
  @Input() currentUser: boolean = false;

  @Input() username: string = '';

  constructor(private auth: AngularFireAuth) { }

  ngOnInit(): void {
    if (this.currentUser) {
      this.auth.user.subscribe(data => {
        this.src = data?.photoURL;
        this.username = data?.displayName || '';
      });
    }
  }
}
