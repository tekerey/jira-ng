import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from "@angular/material/button";
import { MatIconModule } from "@angular/material/icon";
import { MatDialogModule } from "@angular/material/dialog";
import { MatSelectModule } from "@angular/material/select";
import { MatDatepickerModule } from "@angular/material/datepicker";
import { MatInputModule } from "@angular/material/input";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatSnackBarModule } from "@angular/material/snack-bar";
import { MatExpansionModule } from "@angular/material/expansion";
import { MatCardModule } from "@angular/material/card";
import { MatBadgeModule } from "@angular/material/badge";
import { MatSlideToggleModule } from "@angular/material/slide-toggle";
import { MatTooltipModule } from "@angular/material/tooltip";
import { DragDropModule } from "@angular/cdk/drag-drop";

const matComponents = [
  MatButtonModule,
  MatIconModule,
  MatDialogModule,
  MatSelectModule,
  MatDatepickerModule,
  MatInputModule,
  MatFormFieldModule,
  MatSnackBarModule,
  MatExpansionModule,
  MatCardModule,
  MatBadgeModule,
  MatSlideToggleModule,
  DragDropModule,
  MatTooltipModule,
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ...matComponents,
  ],
  exports: [
    ...matComponents,
  ]
})
export class MaterialModule { }
