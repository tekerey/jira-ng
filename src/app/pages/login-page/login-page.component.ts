import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import firebase from 'firebase/app';
import { IUser } from 'src/app/interfaces/user.dto';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {

  constructor(private auth: AngularFireAuth, private router: Router, private firestore: AngularFirestore) { }

  ngOnInit(): void {
  }

  async login() {
    const { user } = await this.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider());
    if (user) {
      const userData: IUser = {
        id: user.uid,
        username: user.displayName,
        userAvatarURL: user.photoURL,
      };
      await this.firestore.doc(`users/${user.uid}`).set(userData);
      this.router.navigateByUrl('/dashboard');
      // this.firestore.collection<IUser>('users').add({
      //   id: user.uid,
      //   username: user.displayName,
      //   userAvatarURL: user.photoURL,
      // });
    }
  }
}
