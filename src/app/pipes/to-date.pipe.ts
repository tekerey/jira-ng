import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'toDate'
})
export class ToDatePipe implements PipeTransform {

  transform(value: {seconds: number}): Date {
    return new Date(value.seconds * 1000);
  }

}
