import { TestBed } from '@angular/core/testing';

import { EditIssueService } from './edit-issue.service';

describe('EditIssueService', () => {
  let service: EditIssueService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EditIssueService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
