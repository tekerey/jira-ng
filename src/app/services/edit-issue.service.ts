import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { IIssue } from '../interfaces/issue.dto';

@Injectable({
  providedIn: 'root'
})
export class EditIssueService {

  issue: IIssue | undefined;
  issueUpdated = new Subject<boolean>();
  issueDeleted = new Subject<boolean>();

  constructor() { }
}
