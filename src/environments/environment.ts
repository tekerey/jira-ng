// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyBH8W4XUpm244hUdIH4j_7BRdbV_t0SAKc',
    authDomain: 'jira-ng.firebaseapp.com',
    //databaseURL: '<your-database-URL>',
    projectId: 'jira-ng',
    storageBucket: 'jira-ng.appspot.com',
    messagingSenderId: '988586220814',
    appId: '1:988586220814:web:123eb712cd16cf800c0b5f',
    //measurementId: '<your-measurement-id>'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
